﻿#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <filesystem>

std::string global_folder;

struct BNode {
    int cnt_keys = 0;
    bool is_leaf = true;
    std::string path; // путь к файлу ноды.

    std::vector<std::pair<int, int>> keys;
    std::vector<std::string> children;
};

void WriteVectors(std::ofstream* output, const std::vector<std::pair<int, int>>& keys,
    const std::vector<std::string>& children) {
    // Записываем размер первого вектора
    int size = keys.size();
    output->write(reinterpret_cast<const char*>(&size), sizeof(size));

    // Записываем элементы первого вектора
    for (auto& item : keys) {
        output->write(reinterpret_cast<const char*>(&item), sizeof(std::pair<int, int>));
    }

    size = children.size();
    output->write(reinterpret_cast<const char*>(&size), sizeof(size));

    // Записываем элементы вектора
    size_t str_size;
    for (const std::string& str : children) {
        str_size = str.size();
        output->write(reinterpret_cast<char*>(&str_size), sizeof(str_size));
        output->write(&str[0], str_size);
    }
}

void ReadVectors(std::ifstream* input, std::vector<std::pair<int, int>>* keys, std::vector<std::string>* children) {
    // Читаем размер перового вектора
    int size = 0;
    input->read(reinterpret_cast<char*>(&size), sizeof(size));

    // Читаем элементы первого вектора
    std::pair<int, int> item;
    for (int i = 0; i < size; ++i) {
        input->read(reinterpret_cast<char*>(&item), sizeof(std::pair<int, int>));
        keys->push_back(item);
    }

    // Читаем размер второго вектора
    input->read(reinterpret_cast<char*>(&size), sizeof(int));

    // Читаем элементы второго вектора
    std::string str;
    size_t str_size;
    for (int i = 0; i < size; ++i) {
        input->read(reinterpret_cast<char*>(&str_size), sizeof(str_size));
        str.resize(str_size);
        input->read(&str[0], str_size);
        children->push_back(str);
    }
}

void DiskWrite(const BNode& node) {
    std::ofstream output{ node.path, std::ofstream::out | std::ofstream::binary };

    output.write(reinterpret_cast<const char*>(&node.cnt_keys), sizeof(node.cnt_keys));
    output.write(reinterpret_cast<const char*>(&node.is_leaf), sizeof(node.is_leaf));
    //output.write(reinterpret_cast<const char*>(&node.path), sizeof(node.path));
    size_t str_size = node.path.size();
    output.write(reinterpret_cast<char*>(&str_size), sizeof(str_size));
    output.write(&node.path[0], str_size);

    WriteVectors(&output, node.keys, node.children);
}

BNode DiskRead(const std::string& path) {
    BNode node;

    std::ifstream input{ path, std::ifstream::binary };

    input.read(reinterpret_cast<char*>(&node.cnt_keys), sizeof(node.cnt_keys));
    input.read(reinterpret_cast<char*>(&node.is_leaf), sizeof(node.is_leaf));
    //input.read(reinterpret_cast<char*>(&node.path), sizeof(node.path));
    std::string str;
    size_t str_size;
    input.read(reinterpret_cast<char*>(&str_size), sizeof(str_size));
    str.resize(str_size);
    input.read(&str[0], str_size);

    node.path = str;

    ReadVectors(&input, &node.keys, &node.children);

    return node;
}

class BTree {
public:
    BTree(size_t t) : t_(t) {
        root.cnt_keys = 0;
        root.is_leaf = true;
        root.path = "root";
        DiskWrite(root);
    }

    static int cnt;

    /**
     * Генерирует путь к ноде.
     * @return Путь к ноде.
     */
    std::string GeneratePath() {
        return global_folder + std::to_string(cnt++);
    }

    /**
     * Ищет элементы в дереве.
     * @param x Корень дерева, в котором ищем.
     * @param k Ключ.
     * @return Пара истина-значение, если ключ есть в дереве, пара ложь-0 - иначе.
     */
    std::pair<bool, int> Search(const BNode& x, int k) { // не было const
        /*if(k == 19){
            LogNode(x);
        }*/
        int i = 0;
        while (i < x.cnt_keys && x.keys[i].first < k) {
            i++;
        }

        if (i < x.cnt_keys && k == x.keys[i].first) {
            return std::make_pair(true, x.keys[i].second);
        }
        else if (x.is_leaf) {
            return std::make_pair(false, 0);
        }
        else {
            if (x.children.size() < i + 1 || x.children[i].length() == 0) {
                return std::make_pair(false, 0);
            }
            BNode xc = DiskRead(x.children[i]);
            return Search(xc, k);
        }
    }

    /**
     * Разбивает ребёнка.
     * @param x Родитель.
     * @param i Индекс ребёнка.
     */
    void SplitChild(BNode& x, int i) {
        BNode y = DiskRead(x.children[i]);
        BNode z{ t_ - 1, y.is_leaf, GeneratePath() };

        for (size_t j = 0; j < t_ - 1; j++) {
            if (z.keys.size() < j + 1) {
                z.keys.push_back(y.keys[j + t_]);
                continue;
            }
            z.keys[j] = y.keys[j + t_];
        }

        if (!y.is_leaf) {
            for (size_t j = 0; j < t_; j++) {
                if (z.children.size() < j + 1) {
                    z.children.push_back(y.children[j + t_]);
                    continue;
                }

                z.children[j] = y.children[j + t_];
            }
            int len = y.children.size() - t_;
            for (int j = 0; j < len; j++) {
                y.children.pop_back();
            }
        }

        y.cnt_keys = t_ - 1;

        x.children.insert(x.children.begin() + 1 + i, z.path);

        x.keys.insert(x.keys.begin() + i, y.keys[t_ - 1]);
        x.cnt_keys++;

        int len = y.keys.size() - (t_ - 1);
        for (int j = 0; j < len; j++) {
            y.keys.pop_back();
        }

        DiskWrite(y);
        DiskWrite(z);
        DiskWrite(x);
    }

    /**
     * Вносит элемент в незаполненный узел.
     * @param x Узел.
     * @param k Пара ключ-значение.
     */
    void InsertNonFull(BNode& x, std::pair<int, int> k) {
        int i = x.cnt_keys - 1;

        // Если лист.
        if (x.is_leaf) {
            // Чтобы было, куда вставлять.
            if (x.keys.size() < x.cnt_keys + 1) {
                x.keys.emplace_back(0, 0);
            }

            while (i >= 0 && k.first < x.keys[i].first) {
                x.keys[i + 1] = x.keys[i];
                i--;
            }

            x.keys[i + 1] = k;
            x.cnt_keys++;
            DiskWrite(x);
        }
        else {
            while (i >= 0 && k.first < x.keys[i].first) {
                i--;
            }
            i++;

            BNode xc;
            if (x.children.size() < i + 1) {
                xc.path = GeneratePath();
                DiskWrite(xc);
                x.children.push_back(xc.path);
            }
            else {
                xc = DiskRead(x.children[i]);
            }

            if (xc.cnt_keys == 2 * t_ - 1) {
                SplitChild(x, i);
                InsertNonFull(x, k);

            }
            else {
                InsertNonFull(xc, k);
            }
        }
    }

    /**
     * Вносит элемент в дерево.
     * @param k Пара ключ-значение.
     */
    void Insert(std::pair<int, int> k) {
        BNode r = root;
        if (root.cnt_keys == 2 * t_ - 1) {
            root.is_leaf = false;
            root.cnt_keys = 0;
            root.path = GeneratePath();
            root.children.clear();
            root.keys.clear();
            root.children.push_back(r.path);

            SplitChild(root, 0);
        }

        InsertNonFull(root, k);
    }

    /**
     * Проверяет, находится ли ключ в текущей ноде.
     * @param x Текущая нода.
     * @param k Ключ.
     * @return Истина, если находится, дожь - если нет.
     */
    bool IsInNode(const BNode& x, int k) {
        for (auto key : x.keys) {
            if (key.first == k) {
                return true;
            }
        }

        return false;
    }

    /**
     * Случай 1 для удаления (когда ключ находится в текущей ноде, являющейся листом).
     * @param node Нода.
     * @param k Ключ.
     * @return Значение.
     */
    int DeleteFromLeaf(BNode& node, int k) {
        for (size_t i = 0; i < node.keys.size(); i++) {
            if (node.keys[i].first == k) {
                int value = node.keys[i].second;
                node.keys.erase(node.keys.begin() + i);
                node.cnt_keys--;
                DiskWrite(node);
                return value;
            }
        }

        return 0;
    }

    /**
     * Случай 2 для удаления (когда ключ находится в текущей ноде, не являющейся листом).
     * @param x Нода.
     * @param k Ключ.
     * @return Значение.
     */
    int DeleteNotFromLeaf(BNode& x, int k) {
        int i = 0;
        while (i < x.cnt_keys && x.keys[i].first < k) {
            i++;
        }

        // Индекс в х и значение ключа.
        int index = i;
        std::pair<int, int> pair = x.keys[i];

        BNode y = DiskRead(x.children[i]); // первый предшествующий ключу дочерний для х узел.
        if (y.keys.size() >= t_) {
            BNode another_child = y;
            while (!another_child.is_leaf) {
                another_child = DiskRead(another_child.children.back());
            }
            x.keys[i] = another_child.keys.back();

            Delete(y, another_child.keys.back().first);
            DiskWrite(x);

            return pair.second;
        }

        BNode z = DiskRead(x.children[i + 1]);
        if (z.keys.size() >= t_) {
            BNode another_child = z;
            while (!another_child.is_leaf) {
                another_child = DiskRead(another_child.children.front());
            }
            x.keys[i] = another_child.keys.front();

            Delete(z, another_child.keys.front().first);
            DiskWrite(x);

            return pair.second;
        }

        y.keys.push_back(pair);
        for (std::pair<int, int> kvs : z.keys) {
            y.keys.push_back(kvs);
        }
        y.cnt_keys += z.keys.size() + 1;

        for (const std::string& child : z.children) {
            y.children.push_back(child);
        }

        x.keys.erase(x.keys.begin() + index);
        x.cnt_keys--;
        x.children.erase(x.children.begin() + i + 1);

        z.children.clear();
        z.keys.clear();
        z.cnt_keys = 0;

        DiskWrite(z);

        Delete(y, k);

        DiskWrite(x);

        return pair.second;
    }

    /**
     * Случай 3 для удаления (когда ключ находится не в текущей ноде).
     * @param x Нода.
     * @param k Ключ.
     * @return Пара ключ-значение.
     */
    std::pair<bool, int> JustDelete(BNode& x, int k) {
        int i = 0;
        while (i < x.cnt_keys && x.keys[i].first < k) {
            i++;
        }

        BNode y = DiskRead(x.children[i]);

        // Случай 3а
        if (y.cnt_keys < t_) {
            if (i > 0) {
                BNode left = DiskRead(x.children[i - 1]);
                if (left.cnt_keys >= t_) {
                    y.keys.insert(y.keys.begin(), x.keys[i - 1]);
                    y.cnt_keys++;

                    x.keys[i - 1] = left.keys.back();
                    left.keys.pop_back();
                    left.cnt_keys--;

                    if (!left.children.empty()) {
                        y.children.insert(y.children.begin(), left.children.back());
                        left.children.pop_back();
                    }

                    DiskWrite(left);
                }
            }
            else if (i < x.cnt_keys - 1) {
                BNode right = DiskRead(x.children[i + 1]);
                if (right.cnt_keys >= t_) {
                    y.keys.push_back(x.keys[i]);
                    y.cnt_keys++;

                    x.keys[i] = right.keys.front();
                    right.keys.erase(right.keys.begin());
                    right.cnt_keys--;

                    if (!right.children.empty()) {
                        y.children.push_back(right.children.front());
                        right.children.erase(right.children.begin());
                    }

                    DiskWrite(right);
                }
            }
        }

        // Случай 3б
        if (y.cnt_keys < t_) {
            if (i > 0) {
                BNode left = DiskRead(x.children[i - 1]);

                y.keys.insert(y.keys.begin(), x.keys[i - 1]);
                y.cnt_keys++;

                y.keys.insert(y.keys.begin(), left.keys.begin(), left.keys.end());
                y.cnt_keys += left.keys.size();

                y.children.insert(y.children.begin(), left.children.begin(), left.children.end());

                left.keys.clear();
                left.cnt_keys = 0;
                left.children.clear();

                DiskWrite(left);

                x.keys.erase(x.keys.begin() + i - 1);
                x.cnt_keys--;
                x.children.erase(x.children.begin() + i - 1);
            }
            else if (i < x.cnt_keys - 1) {
                BNode right = DiskRead(x.children[i + 1]);

                y.keys.push_back(x.keys[i]);
                y.cnt_keys++;

                for (const auto& key : right.keys) {
                    y.keys.push_back(key);
                    y.cnt_keys++;
                }

                for (const auto& child : right.children) {
                    y.children.push_back(child);
                }

                right.keys.clear();
                right.cnt_keys = 0;
                right.children.clear();

                DiskWrite(right);

                x.keys.erase(x.keys.begin() + i);
                x.cnt_keys--;
                x.children.erase(x.children.begin() + i + 1);
            }
        }

        DiskWrite(x);
        return Delete(y, k);
    }

    /**
     * Реализует удаление.
     * @param rooted Корень дерева, из которого удаляем.
     * @param k Ключ.
     * @return Пара истина-значение, если удаление успешное, и пара ложь-0, если нет.
     */
    std::pair<bool, int> Delete(BNode& rooted, int k) {
        if (IsInNode(rooted, k) && rooted.is_leaf) {
            return std::make_pair(true, DeleteFromLeaf(rooted, k));
        }
        else if (IsInNode(rooted, k) && !rooted.is_leaf) {
            return std::make_pair(true, DeleteNotFromLeaf(rooted, k));
        }
        else {
            if (!Search(rooted, k).first) {
                return std::make_pair(false, 0);
            }

            return JustDelete(rooted, k);
        }
    }

    int t_;
    BNode root;
};

const int t = 10;
int BTree::cnt = 0;

int main(int argc, char* argv[]) {
    // Проверка на наличие входных данных.
    if (argc < 4) {
        std::cout << "You haven't inserted input and output files.";
        return -1;
    }

    // Пути к файлам входных и выходных данных.
    std::string folder = argv[1];
    std::string pathin = argv[2];
    std::string pathout = argv[3];

    if (!std::filesystem::exists(folder)) {
        std::filesystem::create_directory(folder);
    }

    if (folder[folder.size() - 1] != '/' && folder[folder.size() - 1] != '\\') {
        folder.push_back('/');
    }

    global_folder = folder;

    try {
        std::ifstream fin{ pathin };
        std::ofstream fout{ pathout };

        // Проверка на существование файла входных данных.
        if (!fin) {
            std::cout << "There is no such input file.";
            return -2;
        }

        BTree tree(t);

        std::string command;
        int cnt = 0;
        while (fin >> command) {
            cnt++;

            if (command == "insert") {
                int key, value;
                fin >> key >> value;
                if (tree.Search(tree.root, key).first) {
                    fout << "false" << std::endl;
                    continue;
                }

                tree.Insert(std::make_pair(key, value));
                fout << "true" << std::endl;

            }
            else if (command == "find") {
                int key;
                fin >> key;

                std::pair<bool, int> found = tree.Search(tree.root, key);
                fout << (found.first ? std::to_string(found.second) : "null") << std::endl;

            }
            else if (command == "delete") {
                int key;
                fin >> key;

                std::pair<bool, int> found = tree.Delete(tree.root, key);
                fout << (found.first ? std::to_string(found.second) : "null") << std::endl;
            }
        }
    }
    catch (const std::exception& ex) {
        std::cout << ex.what() << "\n";
    }


    return 0;
}